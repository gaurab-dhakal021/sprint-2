// from this site
// https://stackoverflow.com/questions/4060004/calculate-age-given-the-birth-date-in-the-format-yyyymmdd
// https://www.javatpoint.com/calculate-age-using-javascript
// console.log("I am here for learning");

function formValidation() {
  ageCalculator();
  telephoneValidation();
}

function ageCalculator() {
  var dateOfBirth = document.getElementById("birthdate").value;
  var dob = new Date(dateOfBirth);
  if (dateOfBirth == null || dateOfBirth == "") {
    document.getElementById("message").innerHTML = "**Choose a date please!";
    return false;
  } else {
    //calculate month difference from current date in time
    var month_diff = Date.now() - dob.getTime();

    //convert the calculated difference in date format
    var age_dt = new Date(month_diff);

    //extract year from date
    var year = age_dt.getUTCFullYear();

    //now calculate the age of the user
    var age = Math.abs(year - 1970);

    //display the calculated age
    // return age;
    if (age > 50) {
      document.getElementById("message").style.display = "none";
      return (document.getElementById("result").innerHTML =
        "<a href='https://www.cdu.edu.au'>Click here for Help</a>");
    } else if (age < 13) {
      document.getElementById("message").style.display = "none";
      return (document.getElementById("result").innerHTML =
        "<span style='color:red'>Sorry you are not eligible</span>");
    } else {
      document.getElementById("message").style.display = "none";
      return (document.getElementById("result").innerHTML =
        "You are eligible!");
    }
  }
}

function telephoneValidation() {
  var telephoneNumber = document.getElementById("telephone").value;
  console.log(telephoneNumber);
  if (telephoneNumber == null || telephoneNumber == "") {
    document.getElementById("phone-message").innerHTML =
      "**Please Enter Country code number";
    return false;
  }
}
